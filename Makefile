.PHONY: qwe

help:
	@echo -e '\033[4m Available commands \033[0m \n'
	@echo -e '\033[1;32m check           \033[0m : Check system health status'
	@echo -e '\033[1;32m coverage        \033[0m : Run code coverage, export coverage result as html and xml'
	@echo -e '\033[1;32m docker          \033[0m : Build and run in docker'
	@echo -e '\033[1;32m install         \033[0m : Install requirements'
	@echo -e '\033[1;32m migrations      \033[0m : Make django migrations'
	@echo -e '\033[1;32m run             \033[0m : Run the project'
	@echo -e '\033[1;32m superuser       \033[0m : Create a super user account on project'
	@echo -e '\033[1;32m test            \033[0m : Run project tests \n'

all: install migrations coverage

check:
	@python manage.py check

coverage:
	@rm -rf htmlcov .coverage coverage.xml
	@coverage run --source='.' manage.py test
	@coverage report -m
	@coverage html
	@coverage xml

install:
	@pip install -r requirements.txt

migrations:
	@python manage.py makemigrations
	@python manage.py migrate

run:
	@python manage.py runserver 127.0.0.1:8000

superuser:
	@python manage.py createsuperuser

test:
	@python manage.py test
