import os

from .base import *

mode = os.getenv('MODE', 'DEV')

if mode == 'PROD':
    from .dev import *
elif mode == 'TEST':
    from .test import *
else:
    from .dev import *
