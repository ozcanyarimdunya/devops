from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .models import Player
from .serializers import PlayerSerializer


class PlayerViewSet(ModelViewSet):
    queryset = Player.objects.all()
    serializer_class = PlayerSerializer

    @action(methods=['get'],
            detail=False,
            url_name='team-codes',
            url_path='team-codes')
    def team_codes(self, request):
        data = [{'code': it[0], 'team': it[1]} for it in Player.TEAM_CHOICES]
        return Response(data, status=status.HTTP_200_OK)
