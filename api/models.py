from django.db import models


class Player(models.Model):
    TEAM_CHOICES = (
        ('ARS', 'Arsenal'),
        ('CHE', 'Chelsea'),
        ('TOT', 'Tottenham'),
        ('LIV', 'Liverpool'),
        ('MCI', 'Manchester City'),
        ('MAU', 'Manchester United'),
    )
    name = models.CharField(max_length=120)
    number = models.IntegerField()
    team = models.CharField(max_length=120, choices=TEAM_CHOICES)

    def __str__(self):
        return "{}/{}".format(self.name, self.number)
