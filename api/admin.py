from django.contrib import admin

from .models import Player


class PlayerAdmin(admin.ModelAdmin):
    list_display = ('name', 'number', 'team')
    list_filter = ('team',)


admin.site.register(Player, PlayerAdmin)
