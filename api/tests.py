from rest_framework.test import APITestCase

from api.models import Player


class TestPlayer(APITestCase):
    def setUp(self) -> None:
        """Call each time any test case called"""

    def tearDown(self) -> None:
        """Call each time any test case finished"""
        Player.objects.all().delete()

    # def test_model(self):
    #     player = Player.objects.create(name='Steven Gerard', number=8, team='LIV')
    #     self.assertEqual(
    #         str(player), 'Steven Gerard/8'
    #     )

    def test_get(self):
        """Test get list of players"""
        Player.objects.create(name='David Beckham', number=7, team='MAU')
        Player.objects.create(name='Son Heung-min', number=7, team='TOT')
        Player.objects.create(name='Mesut Ozil', number=11, team='ARS')
        Player.objects.create(name='Kante', number=12, team='CHE')
        Player.objects.create(name='Sergio Kun Aguero', number=10, team='MCI')
        response = self.client.get('/api/player/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 5)

    def test_create(self):
        """Test create a new player"""
        response = self.client.post(
            '/api/player/',
            data={
                'name': 'Harry Kane',
                'number': 8,
                'team': 'TOT'
            }
        )
        self.assertEqual(
            response.data,
            {'pk': 1, 'name': 'Harry Kane', 'number': 8, 'team': 'TOT'}
        )
        self.assertEqual(response.status_code, 201)

    def test_retrieve(self):
        """Test retrieve single player"""
        player = Player.objects.create(
            name='Paul Pogba',
            number=6,
            team='MAU'
        )

        response = self.client.get(
            '/api/player/{}/'.format(player.pk)
        )
        self.assertEqual(
            response.data,
            {'pk': player.pk, 'name': 'Paul Pogba', 'number': 6, 'team': 'MAU'}
        )
        self.assertEqual(response.status_code, 200)

    def test_update(self):
        """Test update a player"""
        player = Player.objects.create(
            name='Joe Hart',
            number=11,
            team='ARS'
        )

        response = self.client.put(
            '/api/player/{}/'.format(player.pk),
            data={
                'name': 'Joe Hart',
                'number': 1,
                'team': 'TOT'
            }
        )
        self.assertEqual(
            response.data,
            {'pk': player.pk, 'name': 'Joe Hart', 'number': 1, 'team': 'TOT'}
        )
        self.assertEqual(response.status_code, 200)

    def test_partial_update(self):
        """Test patch new user"""
        player = Player.objects.create(
            name='Kevin De Bruyne',
            number=17,
            team='TOT'
        )

        response = self.client.patch(
            '/api/player/{}/'.format(player.pk),
            data={
                'team': 'MCI',
            }
        )
        self.assertEqual(
            response.data,
            {'pk': player.pk, 'name': 'Kevin De Bruyne', 'number': 17, 'team': 'MCI'}
        )
        self.assertEqual(response.status_code, 200)

    def test_delete(self):
        """Test delete a user"""
        player = Player.objects.create(
            name='Sadio Mané',
            number=10,
            team='LIV'
        )

        response = self.client.delete(
            '/api/player/{}/'.format(player.pk)
        )
        self.assertEqual(
            response.status_code, 204
        )

        with self.assertRaises(Player.DoesNotExist):
            Player.objects.get(name='Sadio Mané')

    def test_team_codes(self):
        """Test team codes"""
        response = self.client.get(
            '/api/player/team-codes/'
        )
        self.assertListEqual(
            response.data,
            [
                {'code': 'ARS', 'team': 'Arsenal'},
                {'code': 'CHE', 'team': 'Chelsea'},
                {'code': 'TOT', 'team': 'Tottenham'},
                {'code': 'LIV', 'team': 'Liverpool'},
                {'code': 'MCI', 'team': 'Manchester City'},
                {'code': 'MAU', 'team': 'Manchester United'}
            ]
        )
        self.assertEqual(response.status_code, 200)
